#version 120

uniform sampler2D sampler;
vec4 color;


void main() {

	color = texture2D(sampler, gl_TexCoord[0].xy);

	gl_FragColor = color;

}
